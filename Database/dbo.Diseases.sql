USE [DiseasesApp.Entity]
GO

/****** Object: Table [dbo].[Diseases] Script Date: 2017-04-30 14:21:01 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Diseases] (
    [Id]       INT            IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (MAX) NULL,
    [Type]     NVARCHAR (MAX) NULL,
    [Symptoms] NVARCHAR (MAX) NULL,
    [Cure]     NVARCHAR (MAX) NULL
);


