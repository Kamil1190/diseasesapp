import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
//SHARED
import { SharedModule } from '../shared/shared.module';
//COMPONENTS
//search
import { DiseaseSearchComponent } from './search/diseases.search.component';
//details
import { DiseasesService } from './diseases.service';
import { DiseasesDetailComponent } from './details/diseases.details.component';

@NgModule({
    declarations: [
        DiseasesDetailComponent,
        DiseaseSearchComponent
    ],
    imports: [
        SharedModule,
        RouterModule.forChild([
            { path: 'search', component: DiseaseSearchComponent },
            { path: 'disease/:id', component: DiseasesDetailComponent }
        ])
    ],
    providers: [
        DiseasesService
    ]
})
export class DiseasesModule {

}