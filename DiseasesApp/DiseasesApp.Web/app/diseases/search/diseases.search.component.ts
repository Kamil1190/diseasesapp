import { Component, OnInit } from '@angular/core';
//COMPONENTS
import { DiseasesService } from '../diseases.service';
//MODELS
import { IDisease } from '../../models/disease';
//Rx
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/Rx';


@Component({
    moduleId: module.id,
    templateUrl: 'diseases.search.component.html'
})
export class DiseaseSearchComponent implements OnInit {

    errorMessage: string;
    diseases: IDisease[] = [];
    symptoms: string[] = [];
    typedSymptoms: string = "";

    constructor(private diseasesService: DiseasesService) {
    }

    changeTypedSymptoms(e: any): void {
        this.symptoms = this.typedSymptoms.split(/[ ]+/);
        if (e === 13) {
            this.searchDiseases();
        }
        this.errorMessage = '';
    }

    ngOnInit(): void {

    }

    searchDiseases(): void {
        this.diseasesService.getDiseasesBySymptoms(this.symptoms)
            .subscribe((diseases: IDisease[]) => {
                this.diseases = diseases
                if (this.diseases.length === 0) {
                    this.errorMessage = 'Not found';
                }
            },
            error => this.errorMessage = 'Błąd servera');
    }


}