"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
//COMPONENTS
var diseases_service_1 = require("../diseases.service");
//Rx
// import { Observable } from 'rxjs/Observable';
// import 'rxjs/Rx';
var DiseaseSearchComponent = (function () {
    function DiseaseSearchComponent(diseasesService) {
        this.diseasesService = diseasesService;
        this.diseases = [];
        this.symptoms = [];
        this.typedSymptoms = "";
    }
    DiseaseSearchComponent.prototype.changeTypedSymptoms = function (e) {
        this.symptoms = this.typedSymptoms.split(/[ ]+/);
        if (e === 13) {
            this.searchDiseases();
        }
        this.errorMessage = '';
    };
    DiseaseSearchComponent.prototype.ngOnInit = function () {
    };
    DiseaseSearchComponent.prototype.searchDiseases = function () {
        var _this = this;
        this.diseasesService.getDiseasesBySymptoms(this.symptoms)
            .subscribe(function (diseases) {
            _this.diseases = diseases;
            if (_this.diseases.length === 0) {
                _this.errorMessage = 'Not found';
            }
        }, function (error) { return _this.errorMessage = 'Błąd servera'; });
    };
    return DiseaseSearchComponent;
}());
DiseaseSearchComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'diseases.search.component.html'
    }),
    __metadata("design:paramtypes", [diseases_service_1.DiseasesService])
], DiseaseSearchComponent);
exports.DiseaseSearchComponent = DiseaseSearchComponent;
//# sourceMappingURL=diseases.search.component.js.map