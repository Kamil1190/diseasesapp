import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';
import 'rxjs/add/operator/catch';

import { IDisease } from '../models/disease';

@Injectable()
export class DiseasesService{

    private baseUrl: string = 'http://localhost:55891/';

    constructor(private http: Http){}

    getDiseasesBySymptoms(symptoms: string[]) : Observable<IDisease[]> {

        var headers = new Headers({'Content-Type': 'application/json'});
        var options = new RequestOptions({ headers: headers});

        return this.http.post(this.baseUrl+'disease/search', symptoms, options)
                        .map((res: Response) => <IDisease[]> res.json())
                        .catch(this.errorhandler);
    }

    getDisease(id: number): Observable<IDisease> {

        return this.http.get(this.baseUrl+'disease/'+id)
                        .map((res:Response) => <IDisease> res.json())
                        .catch(this.errorhandler);
    }


    private errorhandler(error: Response){
        console.log(error);
        return Observable.throw(error.json().error || 'Server error');
    }
}