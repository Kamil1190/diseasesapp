"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//COMPONENTS
var diseases_service_1 = require("../diseases.service");
//MODELS
var disease_1 = require("../../models/disease");
var DiseasesDetailComponent = (function () {
    function DiseasesDetailComponent(route, router, diseasesService) {
        this.route = route;
        this.router = router;
        this.diseasesService = diseasesService;
        this.disease = new disease_1.Disease();
    }
    DiseasesDetailComponent.prototype.ngOnInit = function () {
        var _this = this;
        var id = +this.route.snapshot.params['id'];
        this.message = 'Disease nr: ' + id;
        this.diseasesService.getDisease(id).subscribe(function (disease) { return _this.disease = disease; }, function (error) { return _this.message = 'błąd servera'; });
    };
    return DiseasesDetailComponent;
}());
DiseasesDetailComponent = __decorate([
    core_1.Component({
        moduleId: module.id,
        templateUrl: 'diseases.details.component.html'
    }),
    __metadata("design:paramtypes", [router_1.ActivatedRoute, router_1.Router, diseases_service_1.DiseasesService])
], DiseasesDetailComponent);
exports.DiseasesDetailComponent = DiseasesDetailComponent;
//# sourceMappingURL=diseases.details.component.js.map