import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
//COMPONENTS
import { DiseasesService } from '../diseases.service';
//MODELS
import { IDisease, Disease } from '../../models/disease';



@Component({
    moduleId: module.id,
    templateUrl: 'diseases.details.component.html'
})
export class DiseasesDetailComponent implements OnInit {

    message: string;
    disease: IDisease = new Disease();

    constructor(private route: ActivatedRoute, private router: Router, private diseasesService: DiseasesService) {
    
    }
    
    ngOnInit(): void {
        let id = +this.route.snapshot.params['id'];

        this.message = 'Disease nr: '+id;

        this.diseasesService.getDisease(id).subscribe(disease => this.disease = disease, error => this.message = 'błąd servera');
    }

}
