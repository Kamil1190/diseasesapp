"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var router_1 = require("@angular/router");
//SHARED
var shared_module_1 = require("../shared/shared.module");
//COMPONENTS
//search
var diseases_search_component_1 = require("./search/diseases.search.component");
//details
var diseases_service_1 = require("./diseases.service");
var diseases_details_component_1 = require("./details/diseases.details.component");
var DiseasesModule = (function () {
    function DiseasesModule() {
    }
    return DiseasesModule;
}());
DiseasesModule = __decorate([
    core_1.NgModule({
        declarations: [
            diseases_details_component_1.DiseasesDetailComponent,
            diseases_search_component_1.DiseaseSearchComponent
        ],
        imports: [
            shared_module_1.SharedModule,
            router_1.RouterModule.forChild([
                { path: 'search', component: diseases_search_component_1.DiseaseSearchComponent },
                { path: 'disease/:id', component: diseases_details_component_1.DiseasesDetailComponent }
            ])
        ],
        providers: [
            diseases_service_1.DiseasesService
        ]
    })
], DiseasesModule);
exports.DiseasesModule = DiseasesModule;
//# sourceMappingURL=diseases.module.js.map