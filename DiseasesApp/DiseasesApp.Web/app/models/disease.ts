export interface IDisease {
    Id: number;
    Name: string;
    Type: string;
    Symptoms: string;
    Cure: string;
}

export class Disease {
    Id: number;
    Name: string;
    Type: string;
    Symptoms: string;
    Cure: string;
}