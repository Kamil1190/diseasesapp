import { Component } from '@angular/core';

@Component({
    selector: 'pm-app',
    template: `
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <div class="navbar-header">
                     <a class="navbar-brand" href="#"><p class="text-success">Diseases</p></a>
                </div>
                <ul class="nav navbar-nav">
                    <li><a [routerLink]="['/searchDiseases']">Home</a></li>
                </ul>
                <ul class="nav navbar-nav navbar-right">
                    <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                    <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
                </ul>
            </div>
        </nav>
        <div class="container">
            <router-outlet></router-outlet>
        </div>
    `
})
export class AppComponent { }
