﻿using System.Collections.Generic;
using System.Linq;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services;
using DiseasesApp.Tests.Helpers;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DiseasesApp.Tests
{
    /// <summary>
    /// TEST FOR EFService
    /// </summary>
    [TestClass]
    public class EFServiceUnitTest
    {
        [TestMethod]
        public void Can_Increment_Symptoms_By_Attach_To_Disease()
        {
            //prepare
            var mockHelper = new MockHelper();

            var userData = MockDataStructure.Users;
            var mockUser = mockHelper.SetEntityMock(userData);
            var diseaseData = MockDataStructure.DiseaseList;
            var mockDisease = mockHelper.SetEntityMock(diseaseData);
            var patientHasSymptomsData = MockDataStructure.PatientHasSymptomsList;
            var mockPatientHasSymptoms = mockHelper.SetEntityMock(patientHasSymptomsData);
            var mockSymptoms = mockHelper.SetEntityMock(new List<Symptoms>());

            var mockDbContext = new Mock<ApplicationDbContext>();
            mockDbContext.Setup(m => m.Users).Returns(mockUser.Object);
            mockDbContext.Setup(m => m.Disease).Returns(mockDisease.Object);
            mockDbContext.Setup(m => m.PatientHasSymptoms).Returns(mockPatientHasSymptoms.Object);
            mockDbContext.Setup(m => m.Symptoms).Returns(mockSymptoms.Object);

            string patientId = "893ee942-b639-47b9-a831-fda7ecf029c8";
            var disease = diseaseData.First(d => d.Id == 2);

            var service = new EFService(mockDbContext.Object);


            //run
            var result = service.AttachPatientSymptomsToDisease(patientId, disease);


            //assert
            Assert.AreEqual(2, result.Count);
        }

        [TestMethod]
        public void Can_Attach_New_Symptoms_To_Disease()
        {
            //prepare
            var mockHelper = new MockHelper();

            var userData = MockDataStructure.Users;
            var mockUser = mockHelper.SetEntityMock(userData);
            var diseaseData = MockDataStructure.DiseaseList;
            var mockDisease = mockHelper.SetEntityMock(diseaseData);
            var patientHasSymptomsData = MockDataStructure.PatientHasSymptomsList;
            patientHasSymptomsData.Add(new PatientHasSymptoms()
                                        {
                                            Id = 10,
                                            User = new IdentityUser()
                                            {
                                                Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                                            },
                                            Symptom = new Symptom()
                                            {
                                                Id = 7,
                                                Name = "Nudności"
                                            }
                                        });
            var mockPatientHasSymptoms = mockHelper.SetEntityMock(patientHasSymptomsData);
            var mockSymptoms = mockHelper.SetEntityMock(new List<Symptoms>());

            var mockDbContext = new Mock<ApplicationDbContext>();
            mockDbContext.Setup(m => m.Users).Returns(mockUser.Object);
            mockDbContext.Setup(m => m.Disease).Returns(mockDisease.Object);
            mockDbContext.Setup(m => m.PatientHasSymptoms).Returns(mockPatientHasSymptoms.Object);
            mockDbContext.Setup(m => m.Symptoms).Returns(mockSymptoms.Object);

            string patientId = "893ee942-b639-47b9-a831-fda7ecf029c8";
            var disease = diseaseData.First(d => d.Id == 2);

            var service = new EFService(mockDbContext.Object);


            //run
            var result = service.AttachPatientSymptomsToDisease(patientId, disease);


            //assert
            Assert.AreEqual(1, result.Count);
        }


    }
}
