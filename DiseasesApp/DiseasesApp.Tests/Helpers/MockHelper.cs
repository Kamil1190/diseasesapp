﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;

namespace DiseasesApp.Tests.Helpers
{
    class MockHelper
    {
        public Mock<DbSet<T>> SetEntityMock<T>(List<T> data) where T : class
        {
            var mock = new Mock<DbSet<T>>();
            var mockData = data.AsQueryable();

            mock.As<IQueryable<T>>().Setup(m => m.Provider).Returns(mockData.Provider);
            mock.As<IQueryable<T>>().Setup(m => m.Expression).Returns(mockData.Expression);
            mock.As<IQueryable<T>>().Setup(m => m.ElementType).Returns(mockData.ElementType);
            mock.As<IQueryable<T>>().Setup(m => m.GetEnumerator()).Returns(mockData.GetEnumerator());

            return mock;
        }
    }
}
