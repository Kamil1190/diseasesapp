﻿using System.Collections.Generic;
using DiseasesApp.Models.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp.Tests.Helpers
{
    public class MockDataStructure
    {
        public static List<PatientHasSymptoms> PatientHasSymptomsList = new List<PatientHasSymptoms>()
        {
            new PatientHasSymptoms()
            {
                Id = 1,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 1,
                    Name = "Pogorszenie widzenia"
                },
                Symptoms = new Symptoms()
                {
                    Id = 1,
                    Count = 2
                },
            },
            new PatientHasSymptoms()
            {
                Id = 2,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 2,
                    Name = "zaczerwienienie oka"
                },
                Symptoms = new Symptoms()
                {
                    Id = 1,
                    Count = 2
                },
            },
            new PatientHasSymptoms()
            {
                Id = 3,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 1,
                    Name = "Pogorszenie widzenia"
                },
                Symptoms = new Symptoms()
                {
                    Id = 2,
                    Count = 3,
                }
            },
            new PatientHasSymptoms()
            {
                Id = 4,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 2,
                    Name = "zaczerwienienie oka"
                },
                Symptoms = new Symptoms()
                {
                    Id = 2,
                    Count = 3,
                }
            },
            new PatientHasSymptoms()
            {
                Id = 5,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 4,
                    Name = "widzenie auroli"
                },
                Symptoms = new Symptoms()
                {
                    Id = 2,
                    Count = 3,
                }
            },
            new PatientHasSymptoms()
            {
                Id = 6,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 5,
                    Name = "Wymioty"
                },
                Symptoms = new Symptoms()
                {
                    Id = 3,
                    Count = 1,
                }
            },
            new PatientHasSymptoms()
            {
                Id = 7,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 6,
                    Name = "Niestrawność"
                },
                Symptoms = new Symptoms()
                {
                    Id = 3,
                    Count = 1,
                }
            },
            new PatientHasSymptoms()
            {
                Id = 8,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 5,
                    Name = "Wymioty"
                }
            },
            new PatientHasSymptoms()
            {
                Id = 9,
                User = new IdentityUser()
                {
                    Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
                },
                Symptom = new Symptom()
                {
                    Id = 6,
                    Name = "Niestrawność"
                }
            }
        };

        public static List<Disease> DiseaseList = new List<Disease>()
        {
            new Disease()
            {
                Id = 1,
                Name = "Jaskra1",
                Symptoms = "symptomy jaskry",
                Type = "Choroby narzadu wzroku",
                Cure = "leczenie1",
                Symptomses = new List<Symptoms>()
                {
                    new Symptoms()
                    {
                        Id = 1,
                        Count = 2,
                        PatientHasSymptomses = new List<PatientHasSymptoms>()
                        {
                            new PatientHasSymptoms()
                            {
                                Id = 1,
                                Symptom = new Symptom()
                                {
                                    Id = 1,
                                    Name = "Pogorszenie widzenia"
                                }
                            },
                            new PatientHasSymptoms()
                            {
                                Id = 2,
                                Symptom = new Symptom()
                                {
                                    Id = 2,
                                    Name = "zaczerwienienie oka"
                                }
                            }
                        }
                    },
                    new Symptoms()
                    {
                        Id = 2,
                        Count = 3,
                        PatientHasSymptomses = new List<PatientHasSymptoms>()
                        {
                            new PatientHasSymptoms()
                            {
                                Id = 3,
                                Symptom = new Symptom()
                                {
                                    Id = 1,
                                    Name = "Pogorszenie widzenia"
                                }
                            },
                            new PatientHasSymptoms()
                            {
                                Id = 4,
                                Symptom = new Symptom()
                                {
                                    Id = 2,
                                    Name = "zaczerwienienie oka"
                                }
                            },
                            new PatientHasSymptoms()
                            {
                                Id = 5,
                                Symptom = new Symptom()
                                {
                                    Id = 4,
                                    Name = "widzenie auroli"
                                }
                            }
                        }
                    }
                }
            },
            new Disease()
            {
                Id = 2,
                Name = "Zatrucie",
                Symptoms = "jakies symptomy zatrucia",
                Type = "Choroby układu pokarmowego",
                Cure = "leczenie1",
                Symptomses = new List<Symptoms>()
                {
                    new Symptoms()
                    {
                        Id = 3,
                        Count = 1,
                        PatientHasSymptomses = new List<PatientHasSymptoms>()
                        {
                            new PatientHasSymptoms()
                            {
                                Id = 6,
                                Symptom = new Symptom()
                                {
                                    Id = 5,
                                    Name = "Wymioty"
                                }
                            },
                            new PatientHasSymptoms()
                            {
                                Id = 7,
                                Symptom = new Symptom()
                                {
                                    Id = 6,
                                    Name = "Niestrawność"
                                }
                            }
                        }
                    }
                }
            }
        };



        public static List<Symptom> SymptomList = new List<Symptom>()
            {
                //JASKRA
                new Symptom()
                {
                    Id = 1,
                    Name = "Pogorszenie widzenia"
                },
                new Symptom()
                {
                    Id = 2,
                    Name = "zaczerwienienie oka"
                },
                new Symptom()
                {
                    Id = 3,
                    Name = "rozmyta teńczówka"
                },
                new Symptom()
                {
                    Id = 4,
                    Name = "widzenie auroli"
                },
                //ZATRUCIE
                new Symptom()
                {
                    Id = 5,
                    Name = "Wymioty"
                },
                new Symptom()
                {
                    Id = 6,
                    Name = "Niestrawność"
                },
                new Symptom()
                {
                    Id = 7,
                    Name = "Nudności"
                }
            };

        public static List<IdentityUser> Users = new List<IdentityUser>()
        {
            new IdentityUser()
            {
                Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
            }
        };

        public static IdentityUser User = new IdentityUser()
        {
            Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
        };

    }
}
