﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Results;
using AutoMapper;
using DiseasesApp.App_Start;
using DiseasesApp.Controllers;
using DiseasesApp.Data;
using DiseasesApp.Models.DTO;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services.Abstract;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace DiseasesApp.Tests
{
    /// <summary>
    /// TEST FOR DiseasesController
    /// </summary>
    [TestClass]
    public class DiseasesCtrlUnitTest
    {

        [ClassInitialize]
        public static void Init(TestContext context)
        {
            //INITIALIZE AUTOMAPPER FOR TEST
            Mapper.Initialize(c => c.AddProfile<MappingProfile>());
        }

        [TestMethod]
        public void Can_Attach_Patient_Symptoms_To_Disease()
        {
            //prepare
            List<Disease> data = FakeData.GetDiseases();
            DiseaseDto diseaseToAddDto = Mapper.Map<DiseaseDto>(data.First(d => d.Id == 2));
            string patientId = "893ee942-b639-47b9-a831-fda7ecf029c8";

            Mock<IDisease> mockService = new Mock<IDisease>();
            mockService.Setup(s => s.AttachPatientSymptomsToDisease(It.IsAny<string>(), It.IsAny<Disease>())).Returns(new Symptoms());
            DiseaseController controller = new DiseaseController(mockService.Object);

            //run
            var result = controller.AttachSymptoms(patientId, diseaseToAddDto);

            //assert
            var respone = result as OkNegotiatedContentResult<SymptomsDto>;
            Assert.IsNotNull(respone);
            mockService.Verify(m => m.AttachPatientSymptomsToDisease(
                                    It.Is<string>(x => x.Equals(patientId)),
                                    It.Is<Disease>(d => d.Id == diseaseToAddDto.Id && d.Name.Equals(diseaseToAddDto.Name))));
        }
    }
}
