using DiseasesApp.Models.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ApplicationDbContext : IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext()
            : base("name=ApplicationDbContext")
        {
        }


        public virtual DbSet<Disease> Disease { get; set; }
        public virtual DbSet<PatientHasSymptoms> PatientHasSymptoms { get; set; }
        public virtual DbSet<Symptom> Symptom { get; set; }
        public virtual DbSet<Symptoms> Symptoms { get; set; }

        //protected override void OnModelCreating(DbModelBuilder modelBuilder)
        //{
        //    modelBuilder.Entity<Disease>()
        //        .HasMany(e => e.Symptoms1)
        //        .WithOptional(e => e.Disease)
        //        .HasForeignKey(e => e.Disease_Id);

        //    modelBuilder.Entity<Symptom>()
        //        .HasMany(e => e.PatientHasSymptoms)
        //        .WithOptional(e => e.Symptom)
        //        .HasForeignKey(e => e.Symptom_Id);

        //    modelBuilder.Entity<Symptoms>()
        //        .HasMany(e => e.PatientHasSymptoms)
        //        .WithOptional(e => e.Symptoms)
        //        .HasForeignKey(e => e.Symptoms_Id);
        //}
    }
}
