using DiseasesApp.Controllers;
using DiseasesApp.Services;
using DiseasesApp.Services.Abstract;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(DiseasesApp.App_Start.NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(DiseasesApp.App_Start.NinjectWebCommon), "Stop")]

namespace DiseasesApp.App_Start
{
    using System;
    using System.Web;

    using Microsoft.Web.Infrastructure.DynamicModuleHelper;

    using Ninject;
    using Ninject.Web.Common;

    public static class NinjectWebCommon 
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start() 
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }
        
        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }
        
        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();

                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
            //DATABASE
            kernel.Bind<ApplicationDbContext>().ToSelf().InRequestScope();

            //ADMIN
            kernel.Bind<IDisease>().To<EFAdminService>().WhenInjectedInto(typeof(AdminController));
            kernel.Bind<ISymptom>().To<EFAdminService>().WhenInjectedInto(typeof(AdminController));

            //USERS
            kernel.Bind<IDisease>().To<EFService>();
            kernel.Bind<ISymptom>().To<EFService>();
            kernel.Bind<IPatient>().To<EFService>();
            kernel.Bind<ISymptoms>().To<EFService>();
            kernel.Bind<IAccountManagement>().To<EFAccountService>().WhenInjectedInto(typeof(AccountController));
        }
    }
}
