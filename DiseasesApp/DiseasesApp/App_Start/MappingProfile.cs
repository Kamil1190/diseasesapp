﻿using AutoMapper;
using DiseasesApp.Models.DTO;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.App_Start
{
    public class MappingProfile: Profile
    {
        public MappingProfile()
        {
            CreateMap<Disease, DiseaseDto>();
            CreateMap<DiseaseDto, Disease>();

            CreateMap<Symptom, SymptomDto>();
            CreateMap<SymptomDto, Symptom>();

            CreateMap<Symptoms, SymptomsDto>();
            CreateMap<SymptomsDto, Symptoms>();

            CreateMap<PatientHasSymptoms, PatientHasSymptomsDto>();
            CreateMap<PatientHasSymptomsDto, PatientHasSymptoms>();
        }
    }
}