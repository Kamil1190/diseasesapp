﻿using System.Web.Http;
using AutoMapper;
using DiseasesApp.App_Start;
using Ninject.Web.Common;
using Ninject.Web.WebApi;

namespace DiseasesApp
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            config.DependencyResolver = new NinjectDependencyResolver(new Bootstrapper().Kernel);

            Mapper.Initialize(c => c.AddProfile<MappingProfile>());

            config.EnableCors();

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
