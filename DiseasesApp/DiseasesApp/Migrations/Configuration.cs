using System.Collections.Generic;
using System.Linq;
using DiseasesApp.Data;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<DiseasesApp.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DiseasesApp.ApplicationDbContext context)
        {
            context.Disease.AddRange(FakeData.GetDiseases());
            context.SaveChanges();
        }
    }
}
