﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiseasesApp.Models.Entity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp.Data
{
    public static class FakeData
    {

        public static IdentityUser TestUser = new IdentityUser("test")
        {
            Id = "893ee942-b639-47b9-a831-fda7ecf029c8"
        };

        public static List<Symptom> SymptomList = new List<Symptom>()
            {
                //JASKRA
                new Symptom()
                {
                    Id = 1,
                    Name = "Pogorszenie widzenia"
                },
                new Symptom()
                {
                    Id = 2,
                    Name = "zaczerwienienie oka"
                },
                new Symptom()
                {
                    Id = 3,
                    Name = "rozmyta teńczówka"
                },
                new Symptom()
                {
                    Id = 4,
                    Name = "widzenie auroli"
                },
                //ZATRUCIE
                new Symptom()
                {
                    Id = 5,
                    Name = "Wymioty"
                },
                new Symptom()
                {
                    Id = 6,
                    Name = "Niestrawność"
                },
                new Symptom()
                {
                    Id = 7,
                    Name = "Nudności"
                }
            };

        public static List<PatientHasSymptoms> PatientHasSymptomsList = new List<PatientHasSymptoms>()
        {
            new PatientHasSymptoms()
            {
                Id = 1,
                User = TestUser,
                Symptom = SymptomList[0]
            },
            new PatientHasSymptoms()
            {
                Id = 2,
                User = TestUser,
                Symptom = SymptomList[1]
            },
            new PatientHasSymptoms()
            {
                Id = 3,
                User = TestUser,
                Symptom = SymptomList[0]
            },
            new PatientHasSymptoms()
            {
                Id = 4,
                User = TestUser,
                Symptom = SymptomList[1]
            },
            new PatientHasSymptoms()
            {
                Id = 5,
                User = TestUser,
                Symptom = SymptomList[3]
            },
            new PatientHasSymptoms()
            {
                Id = 6,
                User = TestUser,
                Symptom = SymptomList[4]
            },
            new PatientHasSymptoms()
            {
                Id = 7,
                User = TestUser,
                Symptom = SymptomList[5]
            },
            new PatientHasSymptoms()
            {
                Id = 8,
                User = TestUser,
                Symptom = SymptomList[4]
            },
            new PatientHasSymptoms()
            {
                Id = 9,
                User = TestUser,
                Symptom = SymptomList[5]
            }
        };

        public static List<Symptoms> SymptomsList = new List<Symptoms>()
            {
            //JASKRA
                new Symptoms()
                {
                    Id = 1,
                    Count = 2,
                    PatientHasSymptomses = PatientHasSymptomsList.Where(sl => (new int[] { 1, 2 }).Contains(sl.Id)).ToList()
                },
                new Symptoms()
                {
                    Id = 2,
                    Count = 3,
                    PatientHasSymptomses = PatientHasSymptomsList.Where(sl => (new int[] { 3, 4, 5 }).Contains(sl.Id)).ToList()
                },
                //ZATRUCIE
                new Symptoms()
                {
                    Id = 3,
                    Count = 1,
                    PatientHasSymptomses = PatientHasSymptomsList.Where(sl => (new int[] { 6, 7 }).Contains(sl.Id)).ToList()
                }
            };


        public static List<Disease> GetDiseases()
        {
            List<Disease> diseases = new List<Disease>
            {
                new Disease()
                {
                    Id = 1,
                    Name = "Jaskra1",
                    Symptoms = "",
                    Type = "Choroby narzadu wzroku",
                    Cure = "leczenie1",
                    Symptomses = SymptomsList.Where(sl => (new int[] { 1, 2 }).Contains(sl.Id)).ToList()
                },
                new Disease()
                {
                    Id = 2,
                    Name = "Zatrucie",
                    Symptoms = "jakies symptomy",
                    Type = "Choroby układu pokarmowego",
                    Cure = "leczenie1",
                    Symptomses = SymptomsList.Where(sl => (new int[] { 3 }).Contains(sl.Id)).ToList()
                }
            };

            return diseases;
        }



        public static List<PatientHasSymptoms> GetUnattachedPatientSymptoms()
        {
            return PatientHasSymptomsList.Where(sl => (new int[] {8, 9}).Contains(sl.Id)).ToList();
        }

    }
}