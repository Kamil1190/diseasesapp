﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using Castle.Core.Internal;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services.Abstract;

namespace DiseasesApp.Services
{
    public class EFService : IDisease, IPatient, ISymptom, ISymptoms
    {
        protected ApplicationDbContext db;

        public EFService(ApplicationDbContext db)
        {
            this.db = db;
        }

        public IQueryable<Disease> GetDiseasesBySymptoms(IEnumerable<string> symptoms)
        {
            IQueryable<Disease> diseases = db.Disease;
            foreach (var symptom in symptoms)
            {
                diseases = diseases.Where(d => d.Symptoms.Contains(symptom));
            }
            return diseases; 
            //return db.Disease.Where(x => symptoms.Any(y => x.Symptoms.Contains(y)));
        }

        public IQueryable<Disease> GetDiseasesBySymptomsInEntity(IEnumerable<string> symptoms)
        {
            var symptomList = db.Symptom.Where(x => symptoms.Any(s => x.Name.Equals(s)));
            var patientList = db.PatientHasSymptoms.Where(p => symptomList.Contains(p.Symptom));
            var SymptomsList = patientList.Select(p => p.Symptoms);
            var diseaseList = SymptomsList.Select(s => s.Disease);
            return diseaseList;
        }

        public IQueryable<Disease> GetAllDisease()
        {
            return db.Disease;
        }

        public Disease GetDisease(int id)
        {
            return db.Disease.Find(id);
        }

        public IQueryable<Disease> GetPatientDiseases(string idPatient)
        {
            var user = db.Users.Find(idPatient);
            var patientSymptoms = db.PatientHasSymptoms.Where(p => p.User.Id == user.Id);
            var disease = patientSymptoms.Select(p => p.Symptoms).Select(p => p.Disease);
            return disease;
        }

        /// <summary>
        /// Only create new
        /// </summary>
        /// <param name="disease"></param>
        /// <returns></returns>
        public virtual Disease SaveDisease(Disease disease)
        {
            disease.Id = 0;
            db.Disease.Add(disease);
            db.SaveChanges();
            return disease;
        }

        public Disease UpdateDisease(int id, Disease disease)
        {
            var diseaseDb = db.Disease.Find(id);
            if (diseaseDb != null)
            {
                diseaseDb.Name = disease.Name;
                diseaseDb.Type = disease.Type;
                diseaseDb.Cure = disease.Cure;
                diseaseDb.Symptoms = disease.Symptoms;
                db.SaveChanges();
            }
            return diseaseDb;
        }

        /// <summary>
        /// Delete only when disease won't contains symptoms
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual Disease DeleteDisease(int id)
        {
            Disease diseaseDb = db.Disease.Find(id);
            if (diseaseDb != null)
            {
                db.Disease.Remove(diseaseDb);
                db.SaveChanges();
            }
            return diseaseDb;
        }

        public IQueryable<Symptom> GetAllSymptom()
        {
            return db.Symptom;
        }

        public Symptom GetSymptom(int id)
        {
            return db.Symptom.Find(id);
        }

        public Symptom SaveSymptom(Symptom symptom)
        {
            symptom.Id = 0;
            db.Symptom.Add(symptom);
            db.SaveChanges();
            return symptom;
        }

        /// <summary>
        /// update only when symptom doesn't have relation
        /// </summary>
        /// <param name="id"></param>
        /// <param name="symptom"></param>
        /// <returns></returns>
        public virtual Symptom UpdateSymptom(int id, Symptom symptom)
        {
            var symptomDb = db.Symptom.Find(id);
            if (symptom != null)
            {
                if (!db.PatientHasSymptoms.Any(ps => ps.Symptom.Id == symptom.Id))
                {
                    symptomDb.Name = symptom.Name;
                    db.SaveChanges();
                    return symptomDb;
                }
            }
            return null;
        }

        /// <summary>
        /// Delete only when symptoms not belong to diseases
        /// </summary>
        /// <param name="id">Symptom Id</param>
        /// <returns></returns>
        public virtual Symptom DeleteSymptom(int id)
        {
            Symptom symptom = db.Symptom.Find(id);
            if (symptom != null)
            {
                if (!db.Symptoms.Any(s => s.PatientHasSymptomses.Select(p => p.Symptom).Select(ss => ss.Id).Contains(symptom.Id)))
                {
                    db.Symptom.Remove(symptom);
                    db.SaveChanges();
                    return symptom;
                }
            }
            return null;
        }

        /// <summary>
        /// Attach by (update symptoms count / create symptoms and insert disease)
        /// </summary>
        /// <param name="patientId"></param>
        /// <param name="disease"></param>
        /// <returns></returns>
        public Symptoms AttachPatientSymptomsToDisease(string patientId, Disease disease)
        {
            var user = db.Users.FirstOrDefault(u => u.Id.Equals(patientId));
            var diseaseDb = db.Disease.FirstOrDefault(d => d.Id == disease.Id);
            if (user != null && diseaseDb != null)
            {
                //all unassigned user symptoms
                var patientHasSymptomUnassignedQuery = db.PatientHasSymptoms.Where(ps => ps.User.Id.Equals(user.Id)).Where(ps => ps.Symptoms == null);
                var patientHasSymptomUnassigned = patientHasSymptomUnassignedQuery.ToList();

                //if user has symptoms
                if (!patientHasSymptomUnassigned.IsNullOrEmpty())
                {
                    //symptom list for assigned
                    var symptomList = patientHasSymptomUnassignedQuery.Select(ps => ps.Symptom).ToList();
                    //found the same symptoms for this disease
                    var equalDiseaseSymptoms = diseaseDb.Symptomses.FirstOrDefault(s => s.PatientHasSymptomses.Select(ps => ps.Symptom.Id)
                                                                   .SequenceEqual(symptomList.Select(sl => sl.Id)));
                    //if the same
                    if (equalDiseaseSymptoms != null)
                    {
                        equalDiseaseSymptoms.Count++;
                        //attach disease to patient symptoms
                        patientHasSymptomUnassigned.ForEach(ps => ps.Symptoms = equalDiseaseSymptoms);
                        db.SaveChanges();
                        return equalDiseaseSymptoms;
                    }
                    //if this disease hasn't have symptoms
                    else
                    {
                        Symptoms newSymptoms = new Symptoms()
                        {
                            Count = 1,
                            Disease = diseaseDb,
                            PatientHasSymptomses = patientHasSymptomUnassigned
                        };
                        db.Symptoms.Add(newSymptoms);
                        db.SaveChanges();
                        return newSymptoms;
                    }
                }
            }
            return null;
        }

        public IQueryable<Symptom> GetUnattachedPatientSymptoms(string idPatient)
        {
            var user = db.Users.Find(idPatient);
            if (user == null)
            {
                return null;
            }
            var query =
                db.PatientHasSymptoms.Where(ps => ps.User.Id.Equals(user.Id))
                    .Where(ps => ps.Symptoms == null)
                    .Select(ps => ps.Symptom);
            return query;
        }

        public PatientHasSymptoms AttachSymptomToPatient(string idPatient, Symptom symptom)
        {
            var userDb = db.Users.Find(idPatient);
            var symptomDb = db.Symptom.Find(symptom.Id);
            if (userDb == null || symptomDb == null)
            {
                return null;
            }
            bool isExisting = db.PatientHasSymptoms.Any(ps => ps.User.Id == userDb.Id && ps.Symptom.Id == symptomDb.Id);
            if (!isExisting)
            {
                PatientHasSymptoms newPatientSymptom = new PatientHasSymptoms()
                {
                    Symptom = symptomDb,
                    User = userDb
                };
                db.PatientHasSymptoms.Add(newPatientSymptom);
                db.SaveChanges();
                return newPatientSymptom;
            }
            return null;
        }

        public PatientHasSymptoms RemoveSymptomFromPatient(string idPatient, int id)
        {
            var userDb = db.Users.Find(idPatient);
            var symptomDb = db.Symptom.Find(id);
            if (userDb == null || symptomDb == null)
            {
                return null;
            }
            var patientHasSymptom = db.PatientHasSymptoms.FirstOrDefault(ps => ps.User.Id.Equals(idPatient) && ps.Symptom.Id == id);
            if (patientHasSymptom != null)
            {
                db.PatientHasSymptoms.Remove(patientHasSymptom);
                db.SaveChanges();
            }
            return patientHasSymptom;
        }

    }
}