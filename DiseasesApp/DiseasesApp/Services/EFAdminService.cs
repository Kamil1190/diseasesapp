﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Services
{
    public class EFAdminService : EFService
    {

        public EFAdminService(ApplicationDbContext db) : base(db)
        {

        }

        //OVERRIDE DELETE METHODS

        public override Disease DeleteDisease(int id)
        {
            //string firstRole = this.db.Users.FirstOrDefault().Roles.FirstOrDefault().RoleId;

            return base.DeleteDisease(id);
        }

        public override Symptom DeleteSymptom(int id)
        {
            return base.DeleteSymptom(id);
        }


        /// <summary>
        /// update when id != 0, otherway create new
        /// </summary>
        /// <param name="disease"></param>
        /// <returns></returns>
        public override Disease SaveDisease(Disease disease)
        {
            if (disease.Id == 0)
            {
                return base.SaveDisease(disease);
            }
            //update
            throw new NotImplementedException();
        }

        /// <summary>
        /// always update
        /// </summary>
        /// <param name="id"></param>
        /// <param name="symptom"></param>
        /// <returns></returns>
        public override Symptom UpdateSymptom(int id, Symptom symptom)
        {
            throw new NotImplementedException();
        }

    }
}