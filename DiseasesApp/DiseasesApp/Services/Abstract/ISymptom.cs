﻿using System.Linq;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Services.Abstract
{
    public interface ISymptom
    {
        IQueryable<Symptom> GetAllSymptom();
        Symptom GetSymptom(int id);
        Symptom SaveSymptom(Symptom symptom);
        Symptom UpdateSymptom(int id, Symptom symptom);
        Symptom DeleteSymptom(int id);
    }
}
