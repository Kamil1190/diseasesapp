﻿using System.Linq;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Services.Abstract
{
    public interface IPatient
    {
        IQueryable<Symptom> GetUnattachedPatientSymptoms(string idPatient);
        PatientHasSymptoms AttachSymptomToPatient(string idPatient, Symptom symptom);
        PatientHasSymptoms RemoveSymptomFromPatient(string idPatient, int id);
    }
}
