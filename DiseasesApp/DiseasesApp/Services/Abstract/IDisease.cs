﻿using System.Collections.Generic;
using System.Linq;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Services.Abstract
{
    public interface IDisease
    {
        IQueryable<Disease> GetDiseasesBySymptoms(IEnumerable<string> symptoms);
        IQueryable<Disease> GetDiseasesBySymptomsInEntity(IEnumerable<string> symptoms);
        IQueryable<Disease> GetAllDisease();
        IQueryable<Disease> GetPatientDiseases(string idPatient);
        Disease GetDisease(int id);
        Disease SaveDisease(Disease disease);
        Disease UpdateDisease(int id, Disease disease);
        Disease DeleteDisease(int id);
        Symptoms AttachPatientSymptomsToDisease(string patientId, Disease disease);
    }
}
