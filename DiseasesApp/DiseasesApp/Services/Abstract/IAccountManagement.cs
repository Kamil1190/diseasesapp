﻿using System.Collections.Generic;
using System.Threading.Tasks;
using DiseasesApp.Models.DTO;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp.Services.Abstract
{
    public interface IAccountManagement
    {
        Task<IEnumerable<IdentityUser>> Users(string role);
        IdentityUser FindUser(int id);
        Task<IdentityUser> FindUser(string userName, string password);
        Task<IdentityResult> RegisterUser(UserDto userModel);
        void ChangeRoleToPatient(IdentityUser User);
        void ChangeRoleToDoctor(IdentityUser User);

    }
}
