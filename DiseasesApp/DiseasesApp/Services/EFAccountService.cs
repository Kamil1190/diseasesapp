﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using DiseasesApp.Models.DTO;
using DiseasesApp.Services.Abstract;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp.Services
{
    public class EFAccountService : IAccountManagement
    {
        protected UserManager<IdentityUser> userManager;
        protected ApplicationDbContext db;

        public EFAccountService(ApplicationDbContext db)
        {
            this.db = db;
            userManager = new UserManager<IdentityUser>(new UserStore<IdentityUser>(this.db));
        }

        public async Task<IEnumerable<IdentityUser>> Users(string role)
        {
            return await this.db.Users.ToListAsync();
        }

        public IdentityUser FindUser(int id)
        {
            return this.db.Users.Find(id);
        }

        public async Task<IdentityUser> FindUser(string userName, string password)
        {
            return await userManager.FindAsync(userName, password);
        }

        public void ChangeRoleToPatient(IdentityUser User)
        {
            throw new System.NotImplementedException();
        }

        public void ChangeRoleToDoctor(IdentityUser User)
        {
            throw new System.NotImplementedException();
        }

        public async Task<IdentityResult> RegisterUser(UserDto userModel)
        {
            IdentityUser user = new IdentityUser
            {
                UserName = userModel.UserName
            };

            var result = await userManager.CreateAsync(user, userModel.Password);
            return result;
        }

    }
}