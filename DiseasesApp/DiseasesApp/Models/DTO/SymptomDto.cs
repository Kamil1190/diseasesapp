﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiseasesApp.Models.DTO
{
    public class SymptomDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}