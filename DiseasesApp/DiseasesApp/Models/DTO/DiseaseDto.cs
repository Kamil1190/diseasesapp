﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiseasesApp.Models.DTO
{
    public class DiseaseDto
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Symptoms { get; set; }
        public string Cure { get; set; }
    }
}