﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DiseasesApp.Models.Entity;

namespace DiseasesApp.Models.DTO
{
    public class PatientHasSymptomsDto
    {
        public int Id { get; set; }
        public SymptomDto Symptom { get; set; }
    }
}