﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiseasesApp.Models.DTO
{
    public class SymptomsDto
    {
        public int Id { get; set; }
        public int Count { get; set; }
    }
}