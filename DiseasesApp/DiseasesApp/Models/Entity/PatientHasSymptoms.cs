﻿
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity.EntityFramework;

namespace DiseasesApp.Models.Entity
{
    [Table("PatientHasSymptoms")]
    public class PatientHasSymptoms
    {
        public int Id { get; set; }

        public virtual Symptom Symptom { get; set; }
        public virtual Symptoms Symptoms { get; set; }
        public virtual IdentityUser User { get; set; }
    }
}