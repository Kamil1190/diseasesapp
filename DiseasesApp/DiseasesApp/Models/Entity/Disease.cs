﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiseasesApp.Models.Entity
{
    [Table("Disease")]
    public class Disease
    {
        public Disease()
        {
            Symptomses = new List<Symptoms>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Type { get; set; }
        public string Symptoms { get; set; }
        public string Cure { get; set; }

        public virtual ICollection<Symptoms> Symptomses { get; set; }
    }
}
