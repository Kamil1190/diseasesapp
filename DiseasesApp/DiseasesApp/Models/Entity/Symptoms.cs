﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiseasesApp.Models.Entity
{
    [Table("Symptoms")]
    public class Symptoms
    {
        public Symptoms()
        {
            PatientHasSymptomses = new List<PatientHasSymptoms>();
        }
        public int Id { get; set; }
        public int Count { get; set; }

        public virtual Disease Disease { get; set; }
        public virtual ICollection<PatientHasSymptoms> PatientHasSymptomses { get; set; }
    }
}