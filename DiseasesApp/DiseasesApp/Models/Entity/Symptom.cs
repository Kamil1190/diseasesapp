﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DiseasesApp.Models.Entity
{
    [Table("Symptom")]
    public class Symptom
    {
        public Symptom()
        {
            PatientHasSymptomses = new List<PatientHasSymptoms>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PatientHasSymptoms> PatientHasSymptomses { get; set; }
    }
}