﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DiseasesApp.Extensions
{
    public static class Pagination
    {
        //DISEASES
        public static int Max = 100;
        public static int DiseasesDefault = 10;
        public static int SymptomDefault = 5;


        public static IQueryable<T> Paging<T>(this IQueryable<T> source, int pageNumber, int pageLength)
        {
            return source.Skip((pageNumber - 1) * pageLength).Take(pageLength);
        }
    }
}