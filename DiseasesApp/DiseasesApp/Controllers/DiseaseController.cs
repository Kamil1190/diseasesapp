﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Cors;
using AutoMapper;
using DiseasesApp.Extensions;
using DiseasesApp.Models.DTO;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services.Abstract;

namespace DiseasesApp.Controllers
{
    [RoutePrefix("disease")]
    [EnableCors(origins: "http://localhost:3000", headers: "*", methods: "*")]
    public class DiseaseController : ApiController
    {
        private readonly IDisease _serviceDisease;

        public DiseaseController(IDisease serviceDisease)
        {
            this._serviceDisease = serviceDisease;
        }

        // GET http://localhost:55891/disease/page/3
        [HttpGet, Route("page/{page:int:min(1)}")]
        public async Task<IHttpActionResult> GetAll(int page)
        {
            var diseases = _serviceDisease.GetAllDisease().OrderBy(d => d.Name);
            var pagingDiseases = diseases.Paging(page, Pagination.DiseasesDefault);
            return Ok(await pagingDiseases.ToListAsync());
        }

        // GET http://localhost:55891/disease/102
        [HttpGet]
        [Route("{id:int:min(1)}")]
        public IHttpActionResult Get(int id)
        {
            Disease diseaseDb = _serviceDisease.GetDisease(id);
            if (diseaseDb == null)
            {
                return NotFound();
            }
            return Ok(diseaseDb);
        }

        //GET http://localhost:55891/patient/893ee942-b639-47b9-a831-fda7ecf029c8/diseases/1
        [HttpGet]
        [Route("~/patient/{idPatient:guid}/diseases/{page:int:min(1)}")]
        public async Task<IHttpActionResult> GetPatientDiseases(string idPatient, int page)
        {
            var diseases = _serviceDisease.GetPatientDiseases(idPatient).Distinct().OrderBy(d => d.Name);
            var pagingDiseases = diseases.Paging(page, Pagination.DiseasesDefault);
            var diseasesList = await pagingDiseases.ToListAsync();
            var diseasesDtos = diseasesList.Select(Mapper.Map<DiseaseDto>);
            return Ok(diseasesDtos);
        }

        //POST http://localhost:55891/disease/search [ "Pogorszenie widzenia", "zaczerwienienie oka" ]
        [HttpPost]
        [Route("search")]
        public async Task<IHttpActionResult> Search([FromBody] string[] symptoms)
        {
            if (symptoms == null)
            {
                return BadRequest();
            }
            List<string> symptomsList = symptoms.ToList();

            var diseasesText = _serviceDisease.GetDiseasesBySymptoms(symptomsList);
            var diseasesEntity = _serviceDisease.GetDiseasesBySymptomsInEntity(symptomsList);

            var diseases = await diseasesText.Concat(diseasesEntity).Distinct().ToListAsync();

            var result = diseases.Select(Mapper.Map<DiseaseDto>);

            return Ok(result);
        }

        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody] DiseaseDto diseaseDto)
        {
            if (!ModelState.IsValid || diseaseDto == null)
            {
                return BadRequest();
            }
            var disease = _serviceDisease.SaveDisease(Mapper.Map<Disease>(diseaseDto));
            if (disease == null)
            {
                return InternalServerError();
            }
            diseaseDto = Mapper.Map<DiseaseDto>(disease);
            return Ok(diseaseDto);
        }

        [Route("{id:int:min(1)}")]
        public IHttpActionResult Put(int id, [FromBody] DiseaseDto diseaseDto)
        {
            if (!ModelState.IsValid || diseaseDto == null)
            {
                return BadRequest();
            }
            var disease = _serviceDisease.UpdateDisease(id, Mapper.Map<Disease>(diseaseDto));
            if (disease == null)
            {
                return NotFound();
            }
            diseaseDto = Mapper.Map<DiseaseDto>(disease);
            return Ok(diseaseDto);

        }

        [Route("{id:int:min(1)}")]
        public IHttpActionResult Delete(int id)
        {
            var disease = _serviceDisease.DeleteDisease(id);
            if (disease == null)
            {
                return NotFound();
            }
            return Ok();
        }

        [HttpPost]
        [Route("attach/patient/{patientId:guid}")]
        public IHttpActionResult AttachSymptoms(string patientId, [FromBody] DiseaseDto diseaseDto)
        {
            if (!ModelState.IsValid || diseaseDto == null)
            {
                return BadRequest();
            }
            var disease = Mapper.Map<Disease>(diseaseDto);
            Symptoms symptom = _serviceDisease.AttachPatientSymptomsToDisease(patientId, disease);
            if (symptom == null)
            {
                return InternalServerError();
            }
            return Ok(Mapper.Map<SymptomsDto>(symptom));
        }
    }
}
