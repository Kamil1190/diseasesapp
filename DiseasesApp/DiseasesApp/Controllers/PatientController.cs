﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using Castle.Core.Internal;
using DiseasesApp.Models.DTO;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services.Abstract;

namespace DiseasesApp.Controllers
{
    [RoutePrefix("patient")]
    public class PatientController : ApiController
    {
        private readonly IPatient _patientService;

        public PatientController(IPatient patientService)
        {
            _patientService = patientService;
        }

        //GET http://localhost:55891/patient/893ee942-b639-47b9-a831-fda7ecf029c8/symptom
        [HttpGet]
        [Route("{idPatient:guid}/symptom")]
        public async Task<IHttpActionResult> Get(string idPatient)
        {
            var querySymptom = _patientService.GetUnattachedPatientSymptoms(idPatient);
            if (querySymptom.IsNullOrEmpty())
            {
                return BadRequest();
            }
            var listSymptom = await querySymptom.ToListAsync();
            return Ok(listSymptom.Select(s => Mapper.Map<SymptomDto>(s)));
        }

        //POST http://localhost:55891/patient/893ee942-b639-47b9-a831-fda7ecf029c8/symptom
        [HttpPost]
        [Route("{idPatient:guid}/symptom")]
        public IHttpActionResult Post(string idPatient, SymptomDto symptomDto)
        {
            if (!ModelState.IsValid || symptomDto == null)
            {
                return BadRequest();
            } 
            var patientHasSymptom = _patientService.AttachSymptomToPatient(idPatient, Mapper.Map<Symptom>(symptomDto));
            if (patientHasSymptom == null)
            {
                return BadRequest();
            }
            return Ok(Mapper.Map<PatientHasSymptomsDto>(patientHasSymptom));
        }

        //DELETE http://localhost:55891/patient/893ee942-b639-47b9-a831-fda7ecf029c8/symptom/3
        [HttpDelete]
        [Route("{IdPatient:guid}/symptom/{idSymptom:int:min(1)}")]
        public IHttpActionResult Delete(string idPatient, int idSymptom)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest();
            }
            var patientHasSymptom = _patientService.RemoveSymptomFromPatient(idPatient, idSymptom);
            if (patientHasSymptom == null)
            {
                return BadRequest();
            }
            return Ok(patientHasSymptom);
        }
    }
}
