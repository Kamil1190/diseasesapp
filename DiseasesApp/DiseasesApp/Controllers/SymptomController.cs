﻿using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using AutoMapper;
using DiseasesApp.Extensions;
using DiseasesApp.Models.DTO;
using DiseasesApp.Models.Entity;
using DiseasesApp.Services.Abstract;

namespace DiseasesApp.Controllers
{
    [RoutePrefix("symptom")]
    public class SymptomController : ApiController
    {
        private readonly ISymptom _symptomService;

        public SymptomController(ISymptom symptomService)
        {
            _symptomService = symptomService;
        }

        //GET http://localhost:55891/symptom/search/widzenie/1
        [HttpGet]
        [Route("search/{text:alpha}/{page:int:min(1)}")]
        public async Task<IHttpActionResult> Search(string text, int page)
        {
            var symptoms = _symptomService.GetAllSymptom().Where(s => s.Name.Contains(text)).OrderBy(s => s.Name);
            var paginSymptoms = symptoms.Paging(page, Pagination.SymptomDefault);
            var symptomsList = await paginSymptoms.ToListAsync();
            var symptomsDto = symptomsList.Select(ps => Mapper.Map<SymptomDto>(ps));
            return Ok(symptomsDto);
        }

        //GET http://localhost:55891/symptom/1
        [HttpGet]
        [Route("{id:int:min(1)}")]
        public IHttpActionResult Get(int id)
        {
            var symptom = _symptomService.GetSymptom(id);
            if (symptom == null)
            {
                return NotFound();
            }
            return Ok(Mapper.Map<SymptomDto>(symptom));
        }

        //POST http://localhost:55891/symptom/
        [HttpPost]
        [Route("")]
        public IHttpActionResult Post([FromBody] SymptomDto symptomDto)
        {
            if (!ModelState.IsValid && symptomDto == null)
            {
                return BadRequest();
            }
            var symptom = _symptomService.SaveSymptom(Mapper.Map<Symptom>(symptomDto));
            if (symptom == null)
            {
                return InternalServerError();
            }
            symptomDto = Mapper.Map<SymptomDto>(symptom);
            return Ok(symptomDto);
        }

        //PUT http://localhost:55891/symptom/1
        [HttpPut]
        [Route("{id:int:min(1)}")]
        public IHttpActionResult Put(int id, [FromBody] SymptomDto symptomDto)
        {
            if (!ModelState.IsValid || symptomDto == null)
            {
                return BadRequest();
            }
            var symptom = _symptomService.UpdateSymptom(id, Mapper.Map<Symptom>(symptomDto));
            if (symptom == null)
            {
                return InternalServerError();
            }
            return Ok(Mapper.Map<SymptomDto>(symptom));
        }

        //DELETE http://localhost:55891/symptom/1
        [HttpDelete]
        [Route("{id:int:min(1)}")]
        public IHttpActionResult Delete(int id)
        {
            var symptom = _symptomService.DeleteSymptom(id);
            if (symptom == null)
            {
                return NotFound();
            }
            return Ok();
        }
    }
}
