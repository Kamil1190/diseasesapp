﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DiseasesApp.Services.Abstract;

namespace DiseasesApp.Controllers
{
    [RoutePrefix("admin")]
    public class AdminController : ApiController
    {
        private IDisease serviceDisease;
        private ISymptom serviceSymptom;

        public AdminController(IDisease serviceDisease, ISymptom serviceSymptom)
        {
            this.serviceDisease = serviceDisease;
            this.serviceSymptom = serviceSymptom;
        }

        //[Route("delete/{int:id}")]
        public IHttpActionResult Delete(int id)
        {
            serviceDisease.DeleteDisease(id);
            return Ok();
        }

    }
}
