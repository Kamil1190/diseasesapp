﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using DiseasesApp.Models.DTO;
using DiseasesApp.Services;
using DiseasesApp.Services.Abstract;
using Microsoft.AspNet.Identity;

namespace DiseasesApp.Controllers
{
    [RoutePrefix("account")]
    public class AccountController : ApiController
    {
        private readonly IAccountManagement accountService;

        public AccountController(IAccountManagement accountService)
        {
            this.accountService = accountService;
        }

        [HttpPost]
        public async Task<IHttpActionResult> Register(UserDto userModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            IdentityResult result = await accountService.RegisterUser(userModel);

            IHttpActionResult errorResult = GetErrorResult(result);

            if (errorResult != null)
            {
                return errorResult;
            }

            return Ok();
        }







        private IHttpActionResult GetErrorResult(IdentityResult result)
        {
            if (result == null)
            {
                return InternalServerError();
            }

            if (!result.Succeeded)
            {
                if (result.Errors != null)
                {
                    foreach (string error in result.Errors)
                    {
                        ModelState.AddModelError("", error);
                    }
                }

                if (ModelState.IsValid)
                {
                    // No ModelState errors are available to send, so just return an empty BadRequest.
                    return BadRequest();
                }

                return BadRequest(ModelState);
            }

            return null;
        }
    }
}
